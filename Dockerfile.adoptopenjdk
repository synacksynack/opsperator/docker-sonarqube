FROM opsperator/java

# SonarQube server image for OpenShift Origin

ARG DO_UPGRADE=
ARG SONARQUBE_REPO=https://binaries.sonarsource.com/Distribution/sonarqube
ENV SONAR_VERSION=9.9.0.65466 \
    SONARQUBE_HOME=/opt/sonarqube \
    DEBIAN_FRONTEND=noninteractive \
    DESCRIPTION="SonarQube is an open source platform developed by SonarSource for continuous \
inspection of code quality to perform automatic reviews with static analysis of code to detect \
bugs, code smells, and security vulnerabilities on 20+ programming languages. SonarQube offers \
reports on duplicated code, coding standards, unit tests, code coverage, code complexity, \
comments, bugs, and security vulnerabilities."

LABEL description="$DESCRIPTION" \
      io.k8s.description="$DESCRIPTION" \
      io.k8s.display-name="SonarQube $SONAR_VERSION" \
      io.openshift.expose-services="9000:http" \
      io.openshift.tags="ci,sonarqube,sonar" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-sonarqube" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$SONAR_VERSION"

USER root

RUN set -x \
    && echo "# Install SonarQube dependencies" \
    && apt-get update \
    && apt-get -y install postgresql-client ca-certificates curl wget \
	apache2-utils \
    && if test "$DEBUG"; then \
	echo apt-get -y install vim; \
    fi \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && cd /opt \
    && curl --retry 5 --retry-delay 5 -o sonarqube.zip \
	-fSL $SONARQUBE_REPO/sonarqube-$SONAR_VERSION.zip \
    && unzip sonarqube.zip \
    && mv sonarqube-$SONAR_VERSION sonarqube \
    && apt-get clean \
    && rm -rf sonarqube.zip /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ADD config/*.sh $SONARQUBE_HOME/bin/

RUN set -x \
    && rm $SONARQUBE_HOME/bin/nsswrapper.sh \
    && sed -i '/UseConcMarkSweepGC/d' \
	$SONARQUBE_HOME/elasticsearch/config/jvm.options \
    && for i in $SONARQUBE_HOME /etc/ssl \
	    $JAVA_HOME/lib/security/ \
	    $JAVA_HOME/elasticsearch/config/ \
	    /usr/local/share/ca-certificates; \
	do \
	    chown -R 1001:root "$i" \
	    && chmod -R g=u "$i"; \
	done \
    && ln -sf $JAVA_HOME $JAVA_HOME/jre \
    && mv $SONARQUBE_HOME/extensions $SONARQUBE_HOME/extensions-RO \
    && mv /opt/jdk/lib/security /opt/jdk/lib/security-RO \
    && ln -sf $SONARQUBE_HOME/data/extensions $SONARQUBE_HOME/

USER 1001
WORKDIR $SONARQUBE_HOME
ENTRYPOINT ["dumb-init","--","./bin/run_sonarqube.sh"]
