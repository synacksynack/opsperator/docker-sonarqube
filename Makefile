ADMIN_PASSWORD=admin
IMAGE=opsperator/sonarqube
ROOT_DOMAIN=demo.local
SKIP_SQUASH?=1
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: demo
demo:
	@@docker run -p9000:9000 \
	     --tmpfs /opt/jdk/lib/security:exec,size=65536k \
	     --tmpfs /opt/sonarqube/temp:exec,size=512M \
	     --tmpfs /opt/sonarqube/logs:rw,size=65536k \
	     --tmpfs /opt/sonarqube/data:rw,size=512M \
	     -e DEBUG=pleasedo \
	     -e SONAR_PROJECT=cicd \
	     -e ADMIN_PASSWORD=secret42 \
	     -e SCANNER_PASSWORD=secret \
	     -e SCANNER_USER=scanner \
	     -e SONAR_PRIVATE=yes \
	     -it $(IMAGE)

.PHONY: run
run: demo

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in secrets service config deployment ingress; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "SONARQUBE_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "SONARQUBE_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: ocdemo
ocdemo:
	@@oc process -f deploy/openshift/secret.yaml \
	    -p ADMIN_PASSWORD=$(ADMIN_PASSWORD) | oc apply -f-
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p ROOT_DOMAIN=$(ROOT_DOMAIN) | oc apply -f-

.PHONY: ocpersistent
ocpersistent:
	@@oc process -f deploy/openshift/secret.yaml \
	    -p ADMIN_PASSWORD=$(ADMIN_PASSWORD) | oc apply -f-
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p ROOT_DOMAIN=$(ROOT_DOMAIN) | oc apply -f-

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    | oc delete -f- || true

.PHONY: ocpurge
ocpurge:
	@@oc process -f deploy/openshift/build-with-secret.yaml \
	    -p GIT_DEPLOYMENT_TOKEN=abc | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream \
	    | oc delete -f- || true
