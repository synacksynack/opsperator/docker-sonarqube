# k8s SonarQube

OpenShift-friendly SonarQube image, based on
https://github.com/SonarSource/docker-sonarqube

Build with:

```
$ make build
```

Build on OpenShift:

```
$ make ocbuild
```

Deploy ephemeral demo on OpenShift:

```
$ make ROOT_DOMAIN=apps.examplecom ADMIN_PASSWORD=secret ocdemo
```

Clean up deployment:

```
$ make occlean
```

Purge build assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name            |    Description                          | Default                                                 |
| :-------------------------- | --------------------------------------- | ------------------------------------------------------- |
|  `ADMIN_PASSWORD`           | Administrator Password                  | `admin`                                                 |
|  `INSTALL_PLUGINS`          | SonarQube Plugins to install            | undef                                                   |
|  `REMOVE_PLUGINS`           | SonarQube Plugins to purge              | undef                                                   |
|  `SCANNER_PASSWORD`         | Default Scanner Password                | undef                                                   |
|  `SCANNER_USER`             | Default Scanner User                    | undef                                                   |
|  `SONAR_PRIVATE`            | Close Anonymous Accesses to Sonarqube   | undef                                                   |
|  `SONAR_PROJECT`            | Default Project                         | undef                                                   |
|  `SONARQUBE_JDBC_PASSWORD`  | Postgres DB Password                    | undef                                                   |
|  `SONARQUBE_JDBC_URL`       | Postgres DB URL                         | undef, h2 database would be used by default             |
|  `SONARQUBE_JDBC_USERNAME`  | Postgres DB Username                    | undef                                                   |


You can also set the following mount points by passing the
`-v /host:/container` flag to Docker.

|  Volume mount point        | Description                  |
| :------------------------- | ---------------------------- |
|  `/certs`                  | SonarQube CAs to load        |
|  `/opt/sonarqube/conf`     | SonarQube configuration      |
|  `/opt/sonarqube/data`     | SonarQube runtime data       |
|  `/opt/sonarqube/data/es7` | SonarQube es7 (can be reset) |
|  `/opt/sonarqube/logs`     | SonarQube logs               |
|  `/opt/sonarqube/temp`     | SonarQube temp               |
