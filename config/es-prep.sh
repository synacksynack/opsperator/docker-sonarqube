#!/bin/sh

vmmmc=$(sysctl vm.max_map_count 2>/dev/null | awk '{print $3}')
if test "$vmmmc"; then
    if test "$vmmmc" -lt 262144; then
	if ! sysctl -w vm.max_map_count=262144; then
	    echo WARNING: failed raising vm.max_map_count
	    echo WARNING: doing so would be recommended running ElasticSearch
	fi
    fi
fi
