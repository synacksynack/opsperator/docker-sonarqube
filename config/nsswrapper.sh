#!/bin/sh

if test "`id -u`" -ne 0; then
    export USER_ID=$(id -u)
    export GROUP_ID=$(id -g)
    grep -v ^sonar /etc/passwd >/opt/sonarqube/data/passwd
    echo "sonar:x:$USER_ID:$GROUP_ID::/opt/sonarqube/data:/bin/bash" \
	>>/opt/sonarqube/data/passwd
    export NSS_WRAPPER_PASSWD=/opt/sonarqube/data/passwd
    export NSS_WRAPPER_GROUP=/etc/group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
