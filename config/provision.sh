#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if ! test -s $SONARQUBE_HOME/data/current_local_password; then
    echo admin >$SONARQUBE_HOME/data/current_local_password
fi
password=$(head -1 $SONARQUBE_HOME/data/current_local_password | tr -d '\n')
test -z "$ADMIN_PASSWORD" && ADMIN_PASSWORD="$password"

pretty_sleep()
{
    secs="${1:-60}"
    tool="${2:-service}"
    while test "$secs" -gt 0
    do
	/bin/echo -ne "$tool unavailable, sleeping for: $secs\033[0Ks\r"
	sleep 1
	secs=`expr $secs - 1`
    done
}

echo "* Waiting for the SonarQube to become available - this can take a few minutes"
cpt=0
sonar_host=http://127.0.0.1:9000
username=admin
while ! curl -I -s "$sonar_host/" | head -n 1 | awk '{print $2}' | grep 200 >/dev/null
do
    test "$cpt" -ge 60 && break
    pretty_sleep 10 SonarQube
    cpt=`expr $cpt + 1`
done
if ! curl -I -s "$sonar_host/" | head -n 1 | awk '{print $2}' | grep 200 >/dev/null; then
    echo bailing out
    exit 1
fi

echo "* Waiting for SonarQube startup"
cpt=0
while ! curl "$sonar_host/api/system/status" >/dev/null 2>/dev/null
do
    test "$cpt" -ge 60 && break
    pretty_sleep 10 Webserver
    cpt=`expr $cpt + 1`
done

if curl "$sonar_host/api/system/status" 2>/dev/null | grep DB_MIGRATION_NEEDED >/dev/null; then
    echo "* Upgrading Database"
    if ! curl -s -u "$username:$password" -XPOST "$sonar_host/api/system/migrate_db"; then
	echo WARNING: failed requesting DB update
    else
	pretty_sleep 30 "Database Upgrading"
    fi
fi

echo "* Waiting for SonarQube initialization"
cpt=0
while ! curl "$sonar_host/api/system/status" 2>/dev/null | grep '"status":"UP"' >/dev/null
do
    test "$cpt" -ge 60 && break
    pretty_sleep 10 SonarQube initializing
    cpt=`expr $cpt + 1`
done

echo "* Waiting for API to become available"
cpt=0
while ! curl -u "$username:$password" "$sonar_host/api/users/search" 2>/dev/null | grep "\"$username\"" >/dev/null
do
    test "$cpt" -ge 60 && break
    pretty_sleep 10 API
    cpt=`expr $cpt + 1`
done
if ! curl -u "$username:$password" "$sonar_host/api/users/search" 2>/dev/null | grep "\"$username\"" >/dev/null; then
    echo bailing out
    exit 1
fi

sleep 10
cat <<EOF
 == Provisioning Integration API Scripts Starting ==

Publishing and executing on $sonar_host
EOF

if test "$SONAR_PROJECT"; then
    if ! curl -u "$username:$password" "$sonar_host/api/projects/index" 2>/dev/null | grep "\"$SONAR_PROJECT\"" >/dev/null; then
	if ! curl -u "$username:$password" -X POST "$sonar_host/api/projects/create?name=$SONAR_PROJECT&project=$SONAR_PROJECT"; then
	    echo " -- Failed creating project $SONAR_PROJECT"
	fi
    else
	echo " -- Re-using previously existing project $SONAR_PROJECT"
    fi
fi
if test "$SCANNER_USER" -a "$SCANNER_PASSWORD"; then
    if ! curl -u "$username:$password" "$sonar_host/api/users/search" 2>/dev/null | grep "\"$SCANNER_USER\"" >/dev/null; then
        if ! curl -u "$username:$password" -X POST "$sonar_host/api/users/create?login=$SCANNER_USER&name=$SCANNER_USER&password=$SCANNER_PASSWORD"; then
	    echo " -- Failed creating user $SCANNER_USER"
	else
	    for permission in scan provisioning
	    do
		if ! curl -u "$username:$password" -X POST "$sonar_host/api/permissions/add_user?permission=$permission&login=$SCANNER_USER"; then
		    echo " -- Failed setting $permission permission for $SCANNER_USER user"
		fi
	    done
	    if test "$SONAR_PROJECT"; then
		for privilege in scan user
		do
		    if ! curl -u "$username:$password" -X POST "$sonar_host/api/permissions/add_user?permission=$privilege&projectKey=$SONAR_PROJECT&login=$SCANNER_USER"; then
			echo " -- Failed setting $privilege privilege for $SCANNER_USER user on $SONAR_PROJECT project"
		    fi
		done
	    fi
	fi
    else
	echo " -- Re-using previously existing user $SCANNER_USER"
    fi
fi
if test "$SONAR_PRIVATE"; then
    for permission in scan provisioning
    do
	if ! curl -u "$username:$password" -X POST "$sonar_host/api/permissions/remove_group?permission=$permission&groupName=anyone"; then
	    echo " -- Failed disabling $permission permission for unauthenticated users"
	fi
    done
fi
DBHOST=$(echo "$SONARQUBE_JDBC_URL" | sed 's|jdbc:||')
if test "$ADMIN_PASSWORD" != "$password"; then
    if test -z "$SONARQUBE_JDBC_URL"; then
	echo WARNING: admin password reset not implemented with h2
    else
	SALTED=$(htpasswd -bnBC 10 "" "$ADMIN_PASSWORD" | tr -d ':\n' | sed 's/$2y/$2a/')
	echo " -- Setting SonarQube default admin password..."
	PGPASSWORD="$SONARQUBE_JDBC_PASSWORD" psql "-U$SONARQUBE_JDBC_USERNAME" "$DBHOST" -c "UPDATE USERS SET crypted_password='$SALTED', salt=null, hash_method='BCRYPT', reset_password='f' where login = 'admin'"
	if test $? -eq 0; then
	    echo "$ADMIN_PASSWORD" >$SONARQUBE_HOME/data/current_local_password
	    echo " -- Successfully changed admin password"
	else
	    echo " -- Failed setting admin password"
	fi
    fi
fi
if test "$INSTALL_PLUGINS"; then
    if test -z "$SONARQUBE_JDBC_URL"; then
	echo WARNING: external plugins approval not implemented with h2
    elif ! PGPASSWORD="$SONARQUBE_JDBC_PASSWORD" psql "-U$SONARQUBE_JDBC_USERNAME" "$DBHOST" -c "SELECT * FROM properties WHERE prop_key = 'sonar.plugins.risk.consent'" 2>&1 | grep sonar.plugins.risk.consent >/dev/null; then
	if ! PGPASSWORD="$SONARQUBE_JDBC_PASSWORD" psql "-U$SONARQUBE_JDBC_USERNAME" "$DBHOST" -c "INSERT INTO properties VALUES ('AYFTaFffq5kJct5DAJ6o', 'sonar.plugins.risk.consent', 'f', 'ACCEPTED', '', 'xx', '', '')"; then
	    echo WARNING: failed to approve external plugins
	else
	    echo NOTICE: Successfully approved use of external plugins
	fi
    else
	echo NOTICE: external plugins usage already enabled
    fi
fi
if echo "$INSTALL_PLUGINS" | grep -i prometheus >/dev/null; then
    if test -z "$SONARQUBE_JDBC_URL"; then
	echo WARNING: disabling forceAuthentication not implemented with h2
    elif ! PGPASSWORD="$SONARQUBE_JDBC_PASSWORD" psql "-U$SONARQUBE_JDBC_USERNAME" "$DBHOST" -c "SELECT * FROM properties WHERE prop_key = 'sonar.forceAuthentication'" 2>&1 | grep true >/dev/null; then
	if ! PGPASSWORD="$SONARQUBE_JDBC_PASSWORD" psql "-U$SONARQUBE_JDBC_USERNAME" "$DBHOST" -c "UPDATE properties SET text_value='false' WHERE prop_key='sonar.forceAuthentication'"; then
	    echo WARNING: failed enabling public access - required exposing metrics to Prometheus
	else
	    echo NOTICE: Successfully disabled forceAuthentication
	fi
    else
	echo NOTICE: forceAuthentication already disabled
    fi
fi

# TODO / other keys we could set in properties:
#projects.default.visibility
#email.smtp_host.secured
#email.smtp_port.secured
#email.smtp_secure_connection.secured (ssl,starttls,<unset>)
#email.from noreply@nowhere
#email.fromName SonarQube
#email.prefix [SONARQUBE]
#sonar.lf.enableGravatar
#sonar.builtInQualityProfiles.disableNotificationOnUpdate
#sonar.core.serverBaseURL
#sonar.auth.saml.enabled
#sonar.auth.saml.applicationId sonarqube
#sonar.auth.saml.providerName SAML
#sonar.auth.saml.providerId
#sonar.auth.saml.loginUrl
#sonar.auth.saml.certificate.secured <certificate>
#sonar.auth.saml.user.login <login attr>
#sonar.auth.saml.user.name <username attr>
#sonar.auth.saml.user.email <email attr>
#sonar.auth.saml.group.name <group attr>

echo " == Provisioning Scripts Completed =="
