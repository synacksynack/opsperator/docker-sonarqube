#!/bin/sh

if test -d /opt/jdk/lib/security-RO; then
    ls /opt/jdk/lib/security/ >/dev/null 2>&1 || mkdir -p /opt/jdk/lib/security
    ls /opt/jdk/lib/security-RO | while read f
    do
	cat /opt/jdk/lib/security-RO/$f >/opt/jdk/lib/security/$f
    done
fi

should_rehash=false
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	if test -d /etc/pki/ca-trust/source/anchors; then
	    dir=/etc/pki/ca-trust/source/anchors
	else
	    dir=/usr/local/share/ca-certificates
	fi
	d=`echo $f | sed 's|/|-|g'`
	if ! test -s $dir/kube$d; then
	    if ! cat $f >$dir/kube$d; then
		echo WARNING: failed installing $f certificate authority >&2
	    else
		should_rehash=true
	    fi
	fi
    fi
done
if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash

SONARQUBE_TRUST_STORE_SAVE="${SONARQUBE_TRUST_STORE_SAVE:-/opt/sonarqube/data/trusted.jks}"
SONARQUBE_TRUST_STORE_PASS="${SONARQUBE_TRUST_STORE_PASS:-changeit}"
if ! test "$SONARQUBE_TRUST_STORE" -a -s "$SONARQUBE_TRUST_STORE"; then
    if test -d /usr/local/openjdk-11/lib/security; then
	SONARQUBE_TRUST_STORE=/usr/local/openjdk-11/lib/security/cacerts
    else
	SONARQUBE_TRUST_STORE=$JAVA_HOME/lib/security/cacerts
    fi
fi

if test "$RESET_SSL"; then
    echo Reset SonarQube Keystore
    rm -f "$SONARQUBE_TRUST_STORE_SAVE"
fi
if ! test -s "$SONARQUBE_TRUST_STORE_SAVE"; then
    echo Provision SonarQube Keystore
    if ! test -d "$(dirname "$SONARQUBE_TRUST_STORE_SAVE")"; then
	mkdir -p $(dirname "$SONARQUBE_TRUST_STORE_SAVE")
    fi
    cat "$SONARQUBE_TRUST_STORE" >"$SONARQUBE_TRUST_STORE_SAVE"
    count=0
    for ca in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
    do
	if ! test -s "$ca"; then
	    continue
	fi
	old=0
	grep -n 'END CERTIFICATE' "$ca"  | awk -F: '{print $1}' \
	    | while read stop
	    do
		count=`expr $count + 1`
		echo "Processing $ca (#$count)"
		head -$stop "$ca" | tail -`expr $stop - $old` >/tmp/insert.crt
		while :
		do
		    keytool -import -alias inter$count \
			-file /tmp/insert.crt -keystore "$SONARQUBE_TRUST_STORE_SAVE" \
			-storepass "$SONARQUBE_TRUST_STORE_PASS" -noprompt && break
		    count=`expr $count + 1`
		done
		old=$stop
	    done
	echo done with "$ca"
    done
    rm -f /tmp/insert.crt
    unset count old
fi
cp -f "$SONARQUBE_TRUST_STORE_SAVE" "$SONARQUBE_TRUST_STORE"
