#!/bin/sh

set -e

SONARQUBE_TRUST_STORE_SAVE="${SONARQUBE_TRUST_STORE_SAVE:-/opt/sonarqube/data/trusted.jks}"
SONARQUBE_TRUST_STORE_PASS="${SONARQUBE_TRUST_STORE_PASS:-changeit}"

if test -s /usr/local/bin/nsswrapper.sh; then
    export RUNTIME_HOME=$SONARQUBE_HOME
    #export RUNTIME_SHELL=/bin/bash
    export RUNTIME_USER=sonar
    . /usr/local/bin/nsswrapper.sh
else
    . $SONARQUBE_HOME/bin/nsswrapper.sh
fi
. $SONARQUBE_HOME/bin/volume-prep.sh
. $SONARQUBE_HOME/bin/es-prep.sh
. $SONARQUBE_HOME/bin/reset-tls.sh

echo Executing provision.sh
nohup $SONARQUBE_HOME/bin/provision.sh &

if test "$SONARQUBE_JDBC_URL"; then
    exec java -jar lib/sonar-application-$SONAR_VERSION.jar \
	-Dsonar.log.console=true \
	"-Dsonar.jdbc.username=$SONARQUBE_JDBC_USERNAME" \
	"-Dsonar.jdbc.password=$SONARQUBE_JDBC_PASSWORD" \
	"-Dsonar.jdbc.url=$SONARQUBE_JDBC_URL" \
	"-Dsonar.web.javaAdditionalOpts=$SONARQUBE_WEB_JVM_OPTS -Djava.security.egd=file:/dev/./urandom '-Djavax.net.ssl.trustStore=$SONARQUBE_TRUST_STORE_SAVE' '-Djavax.net.ssl.trustStorePassword=$SONARQUBE_TRUST_STORE_PASS'" \
	"$@"
else
    exec java -jar lib/sonar-application-$SONAR_VERSION.jar \
	-Dsonar.log.console=true \
	"-Dsonar.web.javaAdditionalOpts=$SONARQUBE_WEB_JVM_OPTS -Djava.security.egd=file:/dev/./urandom '-Djavax.net.ssl.trustStore=$SONARQUBE_TRUST_STORE_SAVE' '-Djavax.net.ssl.trustStorePassword=$SONARQUBE_TRUST_STORE_PASS'" \
	"$@"
fi
