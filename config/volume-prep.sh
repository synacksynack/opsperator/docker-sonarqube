#!/bin/sh

echo "**** Preparing SonarQube PVC"
if ! test -d /opt/sonarqube/data/extensions; then
    mkdir -p /opt/sonarqube/data/extensions
    ls /opt/sonarqube/extensions-RO 2>/dev/null |
	while read extension
	do
	    echo " ++++ Installing Extension $extension"
	    cp -pr "/opt/sonarqube/extensions-RO/$extension" /opt/sonarqube/data/extensions/
	done
else
    echo " ++++ Resetting ElasticSearch"
    if ! find /opt/sonarqube/data/es7 -name '*lock' -type f -exec rm -f {} \;; then
	echo " +++++ Warning: ES lock files may still be there" >&2
    fi
fi

if test -s /opt/sonarqube/data/.last_seen; then
    if ! grep "^$SONAR_VERSION$" /opt/sonarqube/data/.last_seen >/dev/null; then
	echo " ++++ Resetting plugins running upgrade"
	OLD_VERSION=`cat /opt/sonarqube/data/.last_seen`
	if ! test "$OLD_VERSION"; then
	    OLD_VERSION=last
	elif test -d "/opt/sonarqube/data/extensions-$OLD_VERSION"; then
	    mv "/opt/sonarqube/data/extensions-$OLD_VERSION" \
		"/opt/sonarqube/data/extensions-$OLD_VERSION.last"
	fi
	mv /opt/sonarqube/data/extensions/plugins \
	    "/opt/sonarqube/data/extensions-$OLD_VERSION"
	echo $SONAR_VERSION >/opt/sonarqube/data/.last_seen
    fi
fi
if ! test -d /opt/sonarqube/data/extensions/plugins; then
    rm -f /opt/sonarqube/data/extensions/plugins
    mkdir -p /opt/sonarqube/data/extensions/plugins
fi
if test -d /opt/sonarqube/lib/bundled-plugins; then
    for plugin in /opt/sonarqube/lib/bundled-plugins/*
    do
	plugin_base_name=$(basename ${plugin%-*})
	if test "$(ls /opt/sonarqube/data/extensions/plugins/$plugin_base_name* 2>/dev/null | awk 'END{print NR}')" = 0; then
	    echo "  ++++ Installing plugin $plugin..."
	    cp -p "$plugin" /opt/sonarqube/data/extensions/plugins/
	fi
    done
fi
if test "$INSTALL_PLUGINS"; then
    (
	cd /opt/sonarqube/data/extensions/plugins
	for url in $INSTALL_PLUGINS
	do
	    basename=$(basename "$url")
	    test -f "$basename" && continue
	    echo " ++++ Fetching Extension $basename"
	    wget "$url"
	done
    )
fi
for plugin in $REMOVE_PLUGINS
do
    echo " ++++ Purging Extensions matching $plugin"
    find /opt/sonarqube/data/extensions/plugins -name "*$plugin*" -delete
done

# fail-safe, removing duplicate plugins
lastwas=
for plugin in $(ls -1 /opt/sonarqube/data/extensions/plugins | sort -rn)
do
    checking=$(echo "$plugin" | sed 's|-[0-9]*.*jar||')
    if ! test "$lastwas" = "$checking"; then
	lastwas=$checking
	continue
    fi
    echo " ++++ Purging Extension $plugin"
    rm -vf "/opt/sonarqube/data/extensions/plugins/$plugin"
done

echo "**** Done Preparing PVC"
